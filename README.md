Exsellium LogStreamWrapper for Magento 1.x
==================

This extension allows to redirect log files and error reports to PHP output streams **php://stdout** and **php://stderr** depending on the priority. This is mainly useful for **Docker** environments.

```
LOG FILES:
- Emergency (0) -> php://stderr
- Alert (1) -> php://stderr
- Critical (2) -> php://stderr
- Error (3) -> php://stderr
- Warning (4) -> php://stdout
- Notice (5) -> php://stdout
- Informational (6) -> php://stdout
- Debug (7) -> php://stdout
```
```
REPORTS:
- All reports -> php://stderr

```

Requirements
--------------

![Magento 1.6.x](https://img.shields.io/badge/magento-1.6.x-ef672f.svg?style=flat)
![Magento 1.7.x](https://img.shields.io/badge/magento-1.7.x-ef672f.svg?style=flat)
![Magento 1.8.x](https://img.shields.io/badge/magento-1.8.x-ef672f.svg?style=flat)
![Magento 1.9.x](https://img.shields.io/badge/magento-1.9.x-ef672f.svg?style=flat)

The file _/path/to/your/magento/directory/errors/local.xml_ need to be writable by the web server user (most of the case www-data).

Installation instructions
--------------

#### 1. Disable compilation

Log into Magento Admin Panel and go to _System -> Tools -> Compilation_ and disable the compilation.

#### 2. Install extension

 -  **Modman**

Modman is a module manager for Magento. You can download it on [GitHub]((https://github.com/colinmollenhour/modman)) and use it by running the following commands:
```sh
$ cd /path/to/your/magento/directory
$ modman clone https://bitbucket.org/exsellium/logstreamwrapper-for-magento-1.x.git
```

 - **File transfer**

You can download the last release from [Bitbucket](https://bitbucket.org/exsellium/logstreamwrapper-for-magento-1.x/downloads) and uncompress it to your Magento directory.

#### 3. Clear cache

Go to _System -> Cache Management_ and clear the store cache.

#### 4. Logout and re-login

Log out and back into Magento Admin Panel.

If you have disabled it, you can run the compilation process again.

####5. Set up extension

Go to _System -> Configuration_. You can access the configuration from the tab **EXSELLIUM** and the section **LogStreamWrapper**.

Output examples
--------------

```
LOG FILES:
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX01 said into stdout: "LOG[system.log]: 2017-01-01T00:00:00+00:00 DEBUG (7): System message"
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX01 said into stderr: "LOG[exception.log]: 2017-01-01T00:00:00+00:00 ERR (3): "
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX01 said into stderr: "exception 'Exception' with message 'Exception message' in /path/to/your/magento/directory/index.php:85"
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX01 said into stderr: "Stack trace:"
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX01 said into stderr: "#0 {main}"
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX01 said into stderr: "LOG[custom.log]: 2017-01-01T00:00:00+00:00 CRIT (2): Custom message"
```

```
REPORTS:
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX02 said into stderr: "REPORT[207682918584]: 2017-01-01T00:00:00+00:00: a:4:{i:0;s:94:"SQLSTATE[HY000] [2002] php_network_getaddresses: getaddrinfo failed: Name or service not known";i:1;s:2680:"
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX02 said into stderr: "#0 /path/to/your/magento/directory/lib/Zend/Db/Adapter/Pdo/Mysql.php(111): Zend_Db_Adapter_Pdo_Abstract->_connect()"
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX02 said into stderr: "#1 /path/to/your/magento/directory/lib/Varien/Db/Adapter/Pdo/Mysql.php(396): Zend_Db_Adapter_Pdo_Mysql->_connect()"
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX02 said into stderr: ...
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX02 said into stderr: "#19 /path/to/your/magento/directory/index.php(83): Mage::run('default', 'store')"
[01-Jan-2017 00:00:00] WARNING: [pool www] child XXX02 said into stderr: "#20 {main}";s:3:"url";s:66:"/index.php";s:11:"script_name";s:10:"/index.php";}"
```

License
--------------

Copyright (c) 2016-2017 Exsellium (www.exsellium.com).

Licensed under the Open Software License (OSL 3.0).

See the file LICENSE.txt bundled with this package or the URL http://opensource.org/licenses/osl-3.0.php.