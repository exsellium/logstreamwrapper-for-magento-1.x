<?php
/**
 * Exsellium - LogStreamWrapper for Magento 1.x
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email to
 * license@exsellium.com so we can send you a copy immediately.

 * @category    Exsellium
 * @package     Exsellium_LogStreamWrapper
 * @copyright   Copyright (c) 2016-2017 Exsellium (www.exsellium.com)
 * @license     http://opensource.org/licenses/osl-3.0.php
 */

class Exsellium_LogStreamWrapper_Helper_Data
    extends Mage_Core_Helper_Abstract
{

}