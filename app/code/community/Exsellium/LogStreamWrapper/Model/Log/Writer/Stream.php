<?php
/**
 * Exsellium - LogStreamWrapper for Magento 1.x
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email to
 * license@exsellium.com so we can send you a copy immediately.

 * @category    Exsellium
 * @package     Exsellium_LogStreamWrapper
 * @copyright   Copyright (c) 2016-2017 Exsellium (www.exsellium.com)
 * @license     http://opensource.org/licenses/osl-3.0.php
 */

class Exsellium_LogStreamWrapper_Model_Log_Writer_Stream
    extends Zend_Log_Writer_Stream
{
    /**
     * Redirect log files
     *
     * @var boolean
     */
    protected $_logRedirectFlag = false;


    /**
     * Log file name
     *
     * @var string
     */
    protected $_logFileName = null;

    /**
     * PHP output streams
     *
     * @var array
     */
    protected $_outputStream = array(
        'stdout' => array(
            'uri' => 'php://stdout',
            'mode' => 'w',
            'stream' => null
        ),
        'stderr' => array(
            'uri' => 'php://stderr',
            'mode' => 'w',
            'stream' => null
        )
    );

    /**
     * Class constructor
     *
     * @param mixed $streamOrUrl
     * @param string $mode
     * @throws Zend_Log_Exception
     */
    public function __construct($streamOrUrl, $mode = null)
    {
        parent::__construct($streamOrUrl, $mode);
        $this->_logRedirectFlag = (boolean) Mage::getStoreConfig('logstreamwrapper/settings/log_redirect');

        $this->_setLogFileName($streamOrUrl);
        $this->_openOutputStream();
    }

    /**
     * Set log file name from stream
     *
     * @param mixed $streamOrUrl
     * @throws Zend_Log_Exception
     */
    protected function _setLogFileName($streamOrUrl)
    {
        if ($this->_logRedirectFlag) {
            if (is_array($streamOrUrl) && isset($streamOrUrl['stream'])) {
                $streamOrUrl = $streamOrUrl['stream'];
            }

            if (is_resource($streamOrUrl)) {
                $fileMetaData = stream_get_meta_data($streamOrUrl);

                if ($fileMetaData && isset($fileMetaData['uri'])) {
                    $this->_logFileName = basename($fileMetaData['uri']);
                }
            } else if (is_string($streamOrUrl)) {
                $this->_logFileName = basename($streamOrUrl);
            }

            if (null === $this->_logFileName) {
                throw new Zend_Log_Exception("Cannot determine the log file name");
            }
        }
    }

    /**
     * Open PHP output streams
     *
     * @throws Zend_Log_Exception
     */
    protected function _openOutputStream()
    {
        if ($this->_logRedirectFlag) {
            if (null === $this->_outputStream['stdout']['stream']) {
                $stdoutUri = $this->_outputStream['stdout']['uri'];
                $stdoutMode = $this->_outputStream['stdout']['mode'];
                $this->_outputStream['stdout']['stream'] = @fopen($stdoutUri, $stdoutMode);

                if (false === $this->_outputStream['stdout']['stream']) {
                    throw new Zend_Log_Exception("\"{$stdoutUri}\" cannot be opened with mode \"{$stdoutMode}\"");
                }
            }

            if (null === $this->_outputStream['stderr']['stream']) {
                $stderrUri = $this->_outputStream['stderr']['uri'];
                $stderrMode = $this->_outputStream['stderr']['mode'];
                $this->_outputStream['stderr']['stream'] = @fopen($stderrUri, $stderrMode);

                if (false === $this->_outputStream['stderr']['stream']) {
                    throw new Zend_Log_Exception("\"{$stderrUri}\" cannot be opened with mode \"{$stderrMode}\"");
                }
            }
        }
    }

    /**
     * Set new formatter for this writer
     *
     * @param Zend_Log_Formatter_Interface $formatter
     * @return Zend_Log_Writer_Abstract
     */
    public function setFormatter(Zend_Log_Formatter_Interface $formatter)
    {
        if ($this->_logRedirectFlag) {
            $format = 'LOG[%logFileName%]: %timestamp% %priorityName% (%priority%): %message%' . PHP_EOL;
            $formatter = new Zend_Log_Formatter_Simple($format);
        }

        return parent::setFormatter($formatter);
    }

    /**
     * Close the stream resources
     */
    public function shutdown()
    {
        parent::shutdown();

        if (is_resource($this->_outputStream['stdout']['stream'])) {
            @fclose($this->_outputStream['stdout']['stream']);
            $this->_outputStream['stdout']['stream'] = null;
        }

        if (is_resource($this->_outputStream['stderr']['stream'])) {
            @fclose($this->_outputStream['stderr']['stream']);
            $this->_outputStream['stderr']['stream'] = null;
        }
    }

    /**
     * Write message to the log
     *
     * @param array $event
     * @throws Zend_Log_Exception
     */
    protected function _write($event)
    {
        if ($this->_logRedirectFlag) {
            $this->_stream = null;
            $event['logFileName'] = $this->_logFileName;

            switch ($event['priority']) {
                default:
                case Zend_Log::WARN:
                case Zend_Log::NOTICE:
                case Zend_Log::INFO:
                case Zend_Log::DEBUG:
                    $this->_stream = $this->_outputStream['stdout']['stream'];
                    break;

                case Zend_Log::EMERG:
                case Zend_Log::ALERT:
                case Zend_Log::CRIT:
                case Zend_Log::ERR:
                    $this->_stream = $this->_outputStream['stderr']['stream'];
                    break;
            }
        }

        parent::_write($event);
    }
}