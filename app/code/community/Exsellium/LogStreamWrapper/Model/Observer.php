<?php
/**
 * Exsellium - LogStreamWrapper for Magento 1.x
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email to
 * license@exsellium.com so we can send you a copy immediately.

 * @category    Exsellium
 * @package     Exsellium_LogStreamWrapper
 * @copyright   Copyright (c) 2016-2017 Exsellium (www.exsellium.com)
 * @license     http://opensource.org/licenses/osl-3.0.php
 */

class Exsellium_LogStreamWrapper_Model_Observer
{
    /**
     * Errors directory
     *
     * @var string
     */
    const ERRORS_DIRECTORY = 'errors';

    /**
     * Errors config file
     */
    const ERRORS_LOCAL_XML = 'local.xml';

    /**
     * Update error config
     * {{base_dir}}/errors/local.xml
     *
     * @param Varien_Event_Observer $observer
     * @return  Exsellium_LogStreamWrapper_Model_Observer
     */
    public function updateErrorConfig($observer)
    {
        $reportCopy = (integer) Mage::getStoreConfigFlag('logstreamwrapper/settings/report_copy');
        $reportTrash = (Mage::getStoreConfigFlag('logstreamwrapper/settings/report_delete')) ? 'delete' : 'leave';

        try {
            $errorsDir = Mage::getBaseDir() . DS . self::ERRORS_DIRECTORY;
            $errorsXml = $errorsDir . DS . self::ERRORS_LOCAL_XML;

            if (!file_exists($errorsXml)) {
                $xmlContent = <<<XML
<config>
    <skin>default</skin>
    <report>
        <copy_stderr>$reportCopy</copy_stderr>
        <trash>$reportTrash</trash>
    </report>
</config>
XML;

                if (false === file_put_contents($errorsXml, $xmlContent)) {
                    throw new Mage_Exception("Failed to write data to \"{$errorsXml}\"");
                }

                @chmod($errorsXml, 0644);
            } else {
                $xmlContent = simplexml_load_file($errorsXml);

                if (false !== $xmlContent) {
                    $xmlContent->report->copy_stderr = $reportCopy;
                    $xmlContent->report->trash = $reportTrash;

                    if (false === $xmlContent->asXML($errorsXml)) {
                        throw new Mage_Exception("Failed to write data to \"{$errorsXml}\"");
                    }
                } else {
                    throw new Mage_Exception("Failed to read data from \"{$errorsXml}\"");
                }
            }
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
        }

        return $this;
    }
}