<?php
/**
 * Exsellium - LogStreamWrapper for Magento 1.x
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email to
 * license@exsellium.com so we can send you a copy immediately.

 * @category    Exsellium
 * @package     Exsellium_LogStreamWrapper
 * @copyright   Copyright (c) 2016-2017 Exsellium (www.exsellium.com)
 * @license     http://opensource.org/licenses/osl-3.0.php
 */

class Exsellium_LogStreamWrapper_Error_Processor
    extends Error_Processor
{
    /**
     * Copy error reports
     *
     * @var boolean
     */
    protected $_reportCopyFlag = false;

    /**
     * PHP output streams
     *
     * @var array
     */
    protected $_outputStream = array(
        'stderr' => array(
            'uri' => 'php://stderr',
            'mode' => 'w'
        )
    );

    /**
     * Timestamp format
     *
     * @var string
     */
    protected $_timestampFormat = 'c';

    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_setReportCopyFlag();
    }

    /**
     * Define report copy flag
     */
    protected function _setReportCopyFlag()
    {
        $local = $this->_loadXml(self::MAGE_ERRORS_LOCAL_XML);

        if (isset($local->report->copy_stderr)) {
            $this->_reportCopyFlag = (boolean) $local->report->copy_stderr;
        }
    }

    /**
     * Copy report to PHP STDERR stream
     *
     * @param array $reportData
     * @throws Zend_Exception
     */
    public function copyReportToStderr($reportData)
    {
        if ($this->_reportCopyFlag) {
            $stderrUri = $this->_outputStream['stderr']['uri'];
            $stderrMode = $this->_outputStream['stderr']['mode'];
            $stderrStream = @fopen($stderrUri, $stderrMode);

            if (false !== $stderrStream) {
                $reportData = array_map('strip_tags', $reportData);
                $reportLine = 'REPORT[' .$this->reportId . ']: ' . date($this->_timestampFormat) . ': ' . serialize($reportData);

                if (false === @fwrite($stderrStream, $reportLine)) {
                    @fclose($stderrStream);
                    throw new Zend_Exception("Failed to write error report to \"{$stderrUri}\"");
                }
            } else {
                throw new Zend_Exception("\"{$stderrUri}\" cannot be opened with mode \"{$stderrMode}\"");
            }

            @fclose($stderrStream);

            if ('delete' == $this->_config->trash) {
                @unlink($this->_reportFile);
            }
        }
    }
}