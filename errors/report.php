<?php
/**
 * Exsellium - LogStreamWrapper for Magento 1.x
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email to
 * license@exsellium.com so we can send you a copy immediately.

 * @category    Exsellium
 * @package     Exsellium_LogStreamWrapper
 * @copyright   Copyright (c) 2016-2017 Exsellium (www.exsellium.com)
 * @license     http://opensource.org/licenses/osl-3.0.php
 */

require_once 'processor.php';
$logStreamWrapperFile = MAGENTO_ROOT . '/errors/logstreamwrapper.php';

if (file_exists($logStreamWrapperFile)) {
    require_once $logStreamWrapperFile;
    $processor = new Exsellium_LogStreamWrapper_Error_Processor();
} else {
    $processor = new Error_Processor();
}

if (isset($reportData) && is_array($reportData)) {
    $processor->saveReport($reportData);

    if (is_a($processor, 'Exsellium_LogStreamWrapper_Error_Processor')) {
        $processor->copyReportToStderr($reportData);
    }
}

$processor->processReport();
